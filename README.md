# Triumph Programming Language

An object oriented programming language.


Generating the parser
---------------------

To generate the parser, you need the Gobo "gelex" and "geyacc" tools that are
part of the EiffelStudio distribution. Note that the commands bellow need
environments variables ${ISE_EIFFEL} and ${ISE_PLATFORM} to be defined.

If your system can use the command "make", you can generate every thing using:
```bash
cd project/parser
make
```

To generate the lexical scanner source code "triumph_scanner.e",
use the command:

```bash
cd project/parser
${ISE_EIFFEL}/library/gobo/spec/${ISE_PLATFORM}/bin/gelex triumph_scanner.l
```

To generate the grammar parser source code "triumph_parser.e",
use the command:

```bash
cd project/parser
${ISE_EIFFEL}/library/gobo/spec/${ISE_PLATFORM}/bin/geyacc -o triumph_parser.e -t TRIUMPH_TOKENS -k triumph_tokens.e triumph_parser.y
```

Note that the file "triumph_tokens.e" will also be generated.
